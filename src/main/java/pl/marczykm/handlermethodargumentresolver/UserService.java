package pl.marczykm.handlermethodargumentresolver;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;

@Service
@RequiredArgsConstructor
class UserService {

    private static final String USER_KEY = "user";

    private final HttpSession session;

    User getUser() {
        return (User) session.getAttribute(USER_KEY);
    }

    void setUser(String username) {
        session.setAttribute(USER_KEY, new User(username));
    }

    void removeUser() {
        session.removeAttribute(USER_KEY);
    }

}
