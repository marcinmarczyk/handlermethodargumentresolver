package pl.marczykm.handlermethodargumentresolver;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
class User {
    private String name;
}
