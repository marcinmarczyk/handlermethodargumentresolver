package pl.marczykm.handlermethodargumentresolver;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/choose")
    public ResponseEntity chooseUser(@PathParam("username") String username) {
        userService.setUser(username);
        return ResponseEntity.ok("ok");
    }

    @GetMapping
    public ResponseEntity getUser(User user) throws UserNotLoggedInException {
        if (user == null) {
            throw new UserNotLoggedInException();
        }
        return ResponseEntity.ok(user);
    }

    @GetMapping("/remove")
    public ResponseEntity removeUser() {
        userService.removeUser();
        return ResponseEntity.ok("ok");
    }

}
