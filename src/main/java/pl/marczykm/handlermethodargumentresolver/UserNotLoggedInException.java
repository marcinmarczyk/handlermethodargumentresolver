package pl.marczykm.handlermethodargumentresolver;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
class UserNotLoggedInException extends RuntimeException {
    UserNotLoggedInException() {
        super("User not logged in.");
    }
}
